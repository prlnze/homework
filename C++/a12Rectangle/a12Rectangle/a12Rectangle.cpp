// 52270334 Aphinun Wantanareeyachart
//

#include "stdafx.h"
#include <stdio.h>
#include <string>




class Point
{
public:
	Point::Point(){}
	Point::Point(unsigned int x, unsigned int y){
		setX(x);
		setY(y);
	}
	unsigned int getX()
	{
		return x;
	}
	unsigned int getY()
	{
		return y;
	}
	void setY(unsigned int inputY)
	{
		y = inputY;
	}
	void setX (unsigned int inputX)
	{
		x = inputX;
	}
private:
	unsigned int x;
	unsigned int y;
};

class Rectangle
{
public:
	Rectangle::Rectangle(Point p1, Point p2){
		unsigned int minX = getMinX(p1,p2);
		unsigned int maxX = getMaxX(p1,p2);
		unsigned int minY = getMinY(p1,p2);
		unsigned int maxY = getMaxY(p1,p2);

		lowerLeft = Point(minX,minY);
		upperLeft = Point(minX,maxY);
		upperRight = Point(maxX,maxY);
		lowerRight = Point(maxX,minY);
	}
	Point getUpperLeft(){
		return upperLeft;
	}
	Point getLowerLeft(){
		return lowerLeft;
	}
	Point getUpperRight(){
		return upperRight;
	}
	Point getLowerRight(){
		return lowerRight;
	}

private:
	Point upperLeft;
	Point lowerLeft;
	Point upperRight;
	Point lowerRight;
	unsigned int getMinX(Point p1, Point p2)
	{
		return p1.getX() <= p2.getX() ? p1.getX() : p2.getX();
	}
	unsigned int getMaxX(Point p1, Point p2)
	{
		return p1.getX() >= p2.getX() ? p1.getX() : p2.getX();
	}
	unsigned int getMinY(Point p1, Point p2)
	{
		return p1.getY() <= p2.getY() ? p1.getY() : p2.getY();
	}
	unsigned int getMaxY(Point p1, Point p2)
	{
		return p1.getY() >= p2.getY() ?  p1.getY() : p2.getY();
	}	
};

int area(Rectangle rect){
	int height = rect.getUpperLeft().getY() - rect.getLowerLeft().getY();
	int width = rect.getUpperLeft().getX() - rect.getUpperRight().getX();
	int answer = height*width;
	if (answer < 0){
		answer *= -1;
	}
	return answer;
}

int _tmain(int argc, _TCHAR* argv[])
{
	Point p1 = Point(2,30);
	Point p2 = Point(-5,10);
	Rectangle rect = Rectangle(p1,p2);
	printf("The UpperLeft corner is at (%d,%d), UpperRight is at (%d,%d).\n", rect.getUpperLeft().getX(), rect.getUpperLeft().getY(), rect.getUpperRight().getX(),rect.getUpperRight().getY());
	printf("The LowerLeft corner is at (%d,%d), LowerRight is at (%d,%d).\n", rect.getLowerLeft().getX(), rect.getLowerLeft().getY(),rect.getLowerRight().getX(),rect.getLowerRight().getY());
	printf("The area is %d.\n" , area(rect));
	system("pause");
	return 0;
}