//Suppatta	Jongwattana			52270329
//Apinya	Tuangsubsin			52270333
//Aphinun	Wantanareeyachart	52270334


#include "stdafx.h"
#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

//Person class will store the detail of each person
class Person{
public:
	Person::Person(){}
	Person::Person(string fullname, int age, string gender, string fathername, string mothername){
		setName(fullname);
		setAge(age);
		setGender(gender);
		setFathername(fathername);
		setMothername(mothername);
	}
	string getFullname(){return fullname;}
	int getAge(){return age;}
	string getGender(){return gender;}
	string getFathername(){return fathername;}
	string getMothername(){return mothername;}
	void setAge(int input){age = input;}
	void setName(string input){fullname = input;}
	void setGender(string input){gender = input;}
	void setFathername(string input){
		if(input == "")
			input = "not identified";
		fathername = input;
	}
	void setMothername(string input){
		if(input == "")
			input = "not identified";
		mothername = input;
	}
private:
	string fullname;
	int age;
	string gender;
	string fathername;
	string mothername;
};
//Struct of Node that will tell what things in the Node
struct Node {
        Person * data;
		Node * prev;
        Node * next;
};
//Persons class that will store Person in each Node
//It is using List idea
class Persons {
public:
	Node * currentPointer;
	Node * head;
	Node * tail;

    Persons() {
        currentPointer = 0;
		tail = 0;
		head = 0;
    }

    ~Persons() {
        while(currentPointer != NULL) {
            Node * n = currentPointer->next;
            delete currentPointer;
            currentPointer = n;
        }
    }

    void add(Person * newPerson) {
        Node * n = new Node;
		if(head == 0){
			head = n;
		}
        n->data = newPerson;
		n->next = NULL;
        n->prev = tail;
		if (tail != 0)
			tail->next = n;
		tail = n;
		currentPointer = tail;
    }
	Node * next(){
		Node * nextPointer = currentPointer->next;
		currentPointer = currentPointer->next;
		return nextPointer;
	}
	Node * getCurrentPointer(){
		return currentPointer;
	}
	Node * previous(){
		Node * previousPointer = currentPointer->prev;
		currentPointer = currentPointer->prev;
		return previousPointer;
	}
};

void Add(Persons *);
void Delete(Persons *);
void Find(Persons *);
void Relatives(Persons *);
//Main method. First method to run after run the program
int _tmain(int argc, _TCHAR* argv[])
{
	Persons * persons = new Persons;
	string choice;
	bool isContinue = true;
	do{
		cout << "Family Trees Main Menu:" << endl;
		cout << "(A)dd a new-not existing person" << endl;
		cout << "(D)elete an existing person" << endl;
		cout << "(F)ind and display the details of a person" << endl;
		cout << "show all (R)elatives of a person" << endl;
		cout << "(Q)uit the program" << endl;
		cout << "\nPlease select an operation : ";
		getline(cin,choice);
		if(choice != "")
		switch (tolower(choice[0])){
			default:	cout << "\n\nInvalid input. Please try again\n\n" << endl; break;
		case 'a': Add(persons); break;
		case 'd': Delete(persons); break;
		case 'f': Find(persons); break;
		case 'r': Relatives(persons); break;
		case 'q': isContinue = false; break;
		}
	} while (isContinue);
	return 0;
}
//toLower to convert string to string in lowercase
string toLower(string input){
	string result;
	for (int i = 0; i < input.length() ; i++){
			result += tolower(input[i]);
		}
	return result;
};
//To remove the extra space in the string
string removeSpace(string input){
	string removedspace;
	for (int i = 0; i < input.length() ; i++){
			if (i != 0){
				if(!(input[i] == input[i-1] && input[i] == ' ')){
					removedspace += input[i];
				}
			}else removedspace += input[i];
		}
	return removedspace;
};
//Print out detail of each person
void printPerson(Person * person){
			cout << "\n" << person->getFullname() << ", " << person->getAge() << " years old, " << person->getGender() << ", father : " << person->getFathername() << ", mother : " << person->getMothername() << ".\n" << endl; 
};
//Find node by using name
Node * findNodeByName(string name, Persons * persons){
	name = toLower(removeSpace(name));
	Node * result = 0;
	if(persons->head != 0){
		Node * findPointer = persons->head;
			do{
				string lowername = toLower(findPointer->data->getFullname());
				if (lowername == name){
					result = findPointer;
					break;
				}
				findPointer = findPointer->next;
			}while(findPointer != 0);
	}
	return result;
}
//Collector that collect children search's result from name
Persons * findChildrenNodesByName(string name,Persons * persons){
	name = toLower(removeSpace(name));
	Persons * result = new Persons;
	if(persons->head != 0){
		Node * findPointer = persons->head;
			do{
				string fatherlowername = toLower(findPointer->data->getFathername());
				string motherlowername = toLower(findPointer->data->getMothername());
				if (fatherlowername == name){
					result->add(findPointer->data);
				}else if (motherlowername == name){
					result->add(findPointer->data);
				}
				findPointer = findPointer->next;
			}while(findPointer != 0);
	}
	return result;
};
//Collector that collect sibbling search's result from name
Persons * findSibblingNodesByName(string name,Persons * persons){
	name = toLower(removeSpace(name));
	Node * original = findNodeByName(name, persons);
	string oriFatherName = original->data->getFathername();
	string oriMotherName = original->data->getMothername();
	Persons * result = new Persons();
	if(persons->head != 0){
		Node * findPointer = persons->head;
		do{
			string fathername = findPointer->data->getFathername();
			string mothername = findPointer->data->getMothername();
			if(fathername == oriFatherName && mothername == oriMotherName && name != toLower(findPointer->data->getFullname())){
				result->add(findPointer->data);
			}
			findPointer = findPointer->next;
		}while(findPointer != 0);
	}
	return result;
};
//Print out the ancester(s)
void printAncesters(string name, Persons * persons, int level){
	string prefix = "";
	for(int counter = 0; counter < level; counter++){
		if(counter == 0)
			prefix = prefix + "grand ";
		else
			prefix = "great-" + prefix;
	}
	Node * currentNode = findNodeByName(name,persons);
	if (currentNode != 0){
		Node * fatherNode = findNodeByName(currentNode->data->getFathername(), persons);
		Node * motherNode = findNodeByName(currentNode->data->getMothername(), persons);
		if (fatherNode != 0){
			string relativeWord = prefix+"father";
			cout << fatherNode->data->getFullname() << ", " << relativeWord << endl;
			printAncesters(fatherNode->data->getFullname(),persons,level+1);
		}
		if (motherNode != 0){
			string relativeWord = prefix+"mother";
			cout << motherNode->data->getFullname() << ", " << relativeWord << endl;
			printAncesters(motherNode->data->getFullname(),persons,level+1);
		}
	}
};
//Print out the children
void printChildren(string name, Persons * persons, int level){
	string prefix = "";
	for(int counter = 0; counter < level; counter++){
		if(counter == 0)
			prefix = prefix + "grand ";
		else
			prefix = "great-" + prefix;
	}
	Persons * children = findChildrenNodesByName(name,persons);
	if(children->head != 0){
		string relativeWord = "";
		Node * pointer = children->head;
		while(pointer != 0){
			if(toLower(pointer->data->getGender()) == "male"){
				relativeWord = prefix + "son";
			}else if(toLower(pointer->data->getGender()) == "female"){
				relativeWord = prefix + "daughter";
			}
			cout << pointer->data->getFullname() << ", " << relativeWord << endl;
			if(pointer->next != 0)
				printChildren(pointer->data->getFullname(), persons, level+1);
			pointer = pointer->next;
		};
		
	}
};
//Print out the sibbling(s)
void printSiblings(string name, Persons * persons){
	Persons * siblings = findSibblingNodesByName(name, persons);
	if(siblings != 0){
		Node * original = findNodeByName(name,persons);
		int originalAge = original->data->getAge();
		
		Node * siblingPointer = siblings->head;
		while(siblingPointer != 0){
			string prefix;
			if(siblingPointer->data->getAge() > originalAge){
				prefix += "elder ";
			}else {
				prefix += "younger ";
			}
			if(toLower(siblingPointer->data->getGender()) == "male")
				prefix += "brother";
			else
				prefix += "sister";
			cout << siblingPointer->data->getFullname() << ", " << prefix << endl;
			siblingPointer = siblingPointer->next;
		};
	};
};
//When user select Add menu the program will run this
void Add(Persons * persons){
	string fullname = "";
	string removedspaceToLower = "";
	string age, gender, fathername, mothername;
	char beforeChar = 'a';
	cout << "\nPlease enter a name : ";
	getline(cin,fullname);
	
		fullname = removeSpace(fullname);
		removedspaceToLower = toLower(fullname);
		Node * findResult = findNodeByName(removedspaceToLower, persons);
		if (findResult == 0){
			cout << "\nFullname : " << fullname << endl;
			do {
			cout << "age: ";
			getline(cin, age); 
			} while( age == "" );
			do {
			cout << "gender: ";
			getline(cin,gender);
			} while (gender == "");
			cout << "father's name: ";
			getline(cin,fathername);
			cout << "mother's name: ";
			getline(cin,mothername);
			persons->add(new Person(fullname, atoi(age.c_str()), gender, fathername, mothername));
			cout << "\nWelcome " << fullname << "!\n" << endl;
		}else{ 
			Person * existingPerson = findResult->data;
			cout << "\n" << existingPerson->getFullname() << " exists !" << endl;
			printPerson(existingPerson);
		}
}
//When user select Delete menu the program will run this
void Delete(Persons * persons){
	string fullname;
	Person * person = 0;
	cout << "\nPlease enter a name : ";
	getline(cin, fullname);

	Node * foundNode = findNodeByName(toLower(removeSpace(fullname)), persons);
	if (foundNode != 0)
		person = foundNode->data;
	if (person == 0)
		cout << "\n" << fullname << " is not in this family tree base.\n" << endl;
	else {
		printPerson(person);
		cout << "Are you sure to delete " << person->getFullname() << "? (y/n) :";
		string choice;
		getline(cin, choice);
		switch (tolower(choice[0])){
		default:
		case 'n' : cout << "\n"; break;
		case 'y' : 
			Node * selectedNode = findNodeByName(fullname, persons);
			if(selectedNode == persons->head)
				persons->head = selectedNode->next;
			else if(selectedNode == persons->tail)
				persons->tail = selectedNode->prev;
			else {
				selectedNode->prev->next = selectedNode->next;
				selectedNode->next->prev = selectedNode->prev;
			}
			cout << "\n" << person->getFullname() << " is removed!\n" << endl;
			break;
		}

	}
}
//When user select Find menu the program will run this
void Find(Persons * persons){
	string fullname;
	Person * person = 0;
	cout << "\nPlease enter a name : ";
	getline(cin, fullname);
	Node * foundNode = findNodeByName(fullname, persons);
	if (foundNode !=0){
		printPerson(foundNode->data);
	}else{
		cout << "\n" << fullname << " is not in this family tree base.\n" << endl;
	}
}
//When user select Relatives menu the program will run this
void Relatives(Persons * persons){
	string fullname;
	Person * person = 0;
	cout << "\nPlease enter a name : ";
	getline(cin, fullname);
	int level = 0;
	Node * foundNode = findNodeByName(toLower(removeSpace(fullname)), persons);
	if (foundNode ==0){
		cout << "\n" << fullname << " is not in this family tree base.\n" << endl;
	}else{
		printAncesters(fullname, persons, 0);
		printChildren(fullname, persons, 0);
		printSiblings(fullname, persons);
		cout << "\n";
	}
}

