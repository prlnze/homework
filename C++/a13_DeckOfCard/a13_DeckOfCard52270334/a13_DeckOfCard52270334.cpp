// Aphinun Wantanareeyachart 52270334
//

#include "stdafx.h"
#include <stdio.h>
#include <string>
#include <iostream>
#include <array>
#include <time.h>
#include <sstream>

using namespace std;

// Class Card to store the detail of each card
class Card
{
public:
	Card::Card(){}
	Card::Card(int scoreinput, int shape)
	{
		setScore(scoreinput);
		setShape(shape);
	}
	void setScore(int scoreinput)
	{
		score = scoreinput;
	}
	void setShape(int shapeNumber)
	{
		shape = shapeNumber;
	}
	int getScore()
	{
		return score;
	}
	int getShape()
	{
		return shape;
	}
private:
	int score;
	int shape;
};

//Class Deck used to store 52 cards
class Deck{
public:
	Deck::Deck(){}
	Card cards[52];
	
};

//function that will sum the score from the given cards
int sumScore(Card cards[])
{
	int sum = 0;
	for(int i = 0; i < 3; i++)
	{
		sum += cards[i].getScore();
	}
	return sum;
}

//function that will find the highest card from the given cards
Card findMax(Card cards[])
{
	int maxScore = -1;
	int maxShape = -1;
	for(int i = 0; i < 3 ; i++)
	{
		if (cards[i].getScore() > maxScore)
		{
			maxScore = cards[i].getScore();
			maxShape = cards[i].getShape();
		}else if (cards[i].getScore() == maxScore)
		{
			if(cards[i].getShape() > maxShape)
				maxShape = cards[i].getShape();
		}
	}
	return Card(maxScore, maxShape);
	
}

// function that will print out details of each card from the given cards
void printCards(Card cards[])
{
	for(int i = 0; i < 3;i++)
	{
		string score;
		string shape;
		switch(cards[i].getScore())
		{
			case 1 : score = "A"; break;
			case 10: score = "10"; break;
			case 11 : score = "J"; break;
			case 12 : score = "Q"; break;
			case 13 : score = "K"; break;
			default:  ostringstream ostr; ostr << cards[i].getScore(); score = ostr.str(); break;
		}
		switch(cards[i].getShape())
		{
			case 1: shape = "Club"; break;
			case 2: shape = "Diamond"; break;
			case 3: shape = "Heart"; break;
			case 4: shape = "Spade"; break;
		}
		
		cout << "\n" << i+1 << ". " << score << " (" << shape << ").";
	}
}
int _tmain(int argc, _TCHAR* argv[])
{
	Card cards[6] = {Card()};

	// generate the deck from combining 52 cards
	Deck deck;
	int counter = 0;
	for (int i = 0; i < 13 ; i++)
		for (int j = 0; j < 4 ; j++,counter++)
			deck.cards[counter] = Card(i+1,j+1);

	// random 6 unique number of card from 52 cards in deck
	int randomCardNumber[6];
 	for(int i = 0; i < 6; i++)
	{
		srand(time (NULL));
		int ranNum = rand()%52;
		if(i > 0){
			bool isDuplicate = true;
			while(isDuplicate){
				for(int j = 0; j < i; j++)
				{
					if (randomCardNumber[j] == ranNum){
						ranNum = rand()%52;
						isDuplicate = true;
						break;
					}else
						isDuplicate = false;
				}
			}
			randomCardNumber[i] = ranNum;
		}else
			randomCardNumber[i] = ranNum;
		cards[i] = deck.cards[randomCardNumber[i]];
	}


	cout << "Open cards\n";
	system("pause");
	// The cards on each hand (user//computer)
	Card userCards[3] = {cards[0],cards[1],cards[2]};
	Card comCards[3] = {cards[3],cards[4],cards[5]};

	// Sum the score
	int userScore = sumScore(userCards);
	int comScore = sumScore(comCards);
	string winner;

	// Rules
	if (userScore > 21 && comScore > 21)
		winner = "Draw";
	else if (userScore > 21)
		winner = "Computer";
	else if (comScore > 21)
		winner = "User";
	else if (comScore > userScore)
		winner = "Computer";
	else if (comScore < userScore)
		winner = "User";
	else if (comScore == userScore)
	{
		Card userHighcard = findMax(userCards);
		Card comHighcard = findMax(comCards);
		int userMaxScore = userHighcard.getScore();
		int comMaxScore = comHighcard.getScore();
		if(userMaxScore > comMaxScore)
			winner = "User";
		else if(userMaxScore < comMaxScore)
			winner = "Computer";
		else if(userMaxScore == comMaxScore)
		{
			if(userHighcard.getShape() > comHighcard.getShape())
				winner = "User";
			else
				winner = "Computer";
		}
	}
	
	printCards(userCards);
	cout << "\nUser Score is : " << userScore << "\n======================================" << endl;
	printCards(comCards);
	cout << "\nComputer Score is : " << comScore << "\n======================================" << endl;
	if (winner != "Draw")
		cout << "Winner is : " << winner << endl;
	else
		cout << "This game is Draw" << endl;
	
	system("pause");
	return 0;
}
